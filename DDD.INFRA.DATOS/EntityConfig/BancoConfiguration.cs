﻿using DDD.DOMINIO.Entidad;
using System.Data.Entity.ModelConfiguration;

namespace DDD.INFRA.DATOS.EntityConfig
{
    public class BancoConfiguration: EntityTypeConfiguration<Banco>
    {
        public BancoConfiguration()
        {
            HasKey(t => t.Id);
            Property(t => t.Acronymus).IsRequired().HasMaxLength(20);
            Property(t => t.Name).IsRequired().HasMaxLength(30);
            Property(t => t.TipoBancoId).IsRequired().HasColumnName("TipoBancoId");
        }
    }
}

﻿using DDD.DOMINIO.Entidad;
using DDD.DOMINIO.Interface;
using DDD.DOMINIO.Interface.Service;

namespace DDD.DOMINIO.Servicio
{
    public class TipoBancoService: ServiceBase<TipoBanco>, ITipoBancoService
    {
        private readonly ITipoBancoRepository _tipoBancoRespository;

        public TipoBancoService(ITipoBancoRepository tipoBancoRepository)
            : base(tipoBancoRepository)
        {
            _tipoBancoRespository = tipoBancoRepository;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DDD.DOMINIO.Entidad
{
    [Table("TiposdeBancos")]
    public class TipoBanco
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Se requiere el {0} del campo")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }


        //Campos de control

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        [DataType(DataType.Date)]
        [Required]
        [ScaffoldColumn(false)]
        public DateTime DataCreation { get; set; }

        [DataType(DataType.Date)]
        [Required]
        [ScaffoldColumn(false)]
        public DateTime DataModification { get; set; }

        public virtual IEnumerable<Banco> Bancos { get; set; }
    }
}

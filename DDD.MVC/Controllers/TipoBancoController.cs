﻿using AutoMapper;
using DDD.DOMINIO.Entidad;
using DDD.INFRA.DATOS.Repositorios;
using DDD.MVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DDD.MVC.Controllers
{
    public class TipoBancoController : Controller
    {
        private readonly TipoBancoRepository _tipoBancoRepository = new TipoBancoRepository();


        // GET: TipoBanco
        public ActionResult Index()
        {
            var tipoBancoViewModel = Mapper.Map<IEnumerable<TipoBanco>, IEnumerable<TipoBancoViewModel>>(_tipoBancoRepository.GetAll());
            return View(tipoBancoViewModel);
        }

        // GET: TipoBanco/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TipoBanco/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoBanco/Create
        [HttpPost]
        public ActionResult Create(TipoBancoViewModel tipoBanco)
        {
            if (ModelState.IsValid)
            {
                var tipoBancoDomain = Mapper.Map<TipoBancoViewModel, TipoBanco>(tipoBanco);
                _tipoBancoRepository.Add(tipoBancoDomain);
                return RedirectToAction("Index");
            }
            return View(tipoBanco);
        }

        // GET: TipoBanco/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TipoBanco/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TipoBanco/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TipoBanco/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using AutoMapper;
using DDD.DOMINIO.Entidad;
using DDD.MVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDD.MVC.AutoMapper
{
    public class ViewModelToDomainMappingProfile: Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMapping"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<TipoBanco, TipoBancoViewModel> ();
            Mapper.CreateMap<Banco, BancoViewModel> ();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.DOMINIO.Entidad
{

    [Table("Banco")]
    public class Banco
    { 
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Se requiere  {0}")]
        [Display(Name = "Tipo de Banco")]
        public int TipoBancoId { get; set; }

        [ForeignKey("TipoBancoId")]
        public virtual TipoBanco TipoBanco { get; set; }

        [Required(ErrorMessage ="Se requiere {0} del campo")]
        [Display(Name="Abreviatura")]
        public string Acronymus { get; set; }

        [Required(ErrorMessage = "Se requiere el {0} del campo")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        
        //Campos de control

        [Display(Name="Activo")]
        public bool Active { get; set; }

        [DataType(DataType.Date)]
        [Required]
        [ScaffoldColumn(false)]
        public DateTime DataCreation { get; set; }

        [DataType(DataType.Date)]
        [Required]
        [ScaffoldColumn(false)]
        public DateTime DataModification { get; set; }
    }
}

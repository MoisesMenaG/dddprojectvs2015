﻿using AutoMapper;
using DDD.DOMINIO.Entidad;
using DDD.MVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDD.MVC.AutoMapper
{
    public class DomainToViewModelMappingProfile: Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMapping"; }
        }

        protected override  void Configure()
        {
            Mapper.CreateMap<TipoBancoViewModel, TipoBanco> ();
            Mapper.CreateMap<BancoViewModel, Banco> ();
        }
    }
}
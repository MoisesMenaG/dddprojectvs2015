﻿using DDD.DOMINIO.Entidad;
using DDD.DOMINIO.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.INFRA.DATOS.Repositorios
{
    public class BancoRepository: RepositoryBase<Banco>, IBancoRepository
    {
    }
}

﻿using DDD.DOMINIO.Entidad;
using DDD.INFRA.DATOS.EntityConfig;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.INFRA.DATOS.Contexto
{
    public class ModeloContexto : DbContext
    {
        public ModeloContexto(): base("DefaultConexion")
        {}

        public DbSet<Banco> Bancos { get; set; }
        public DbSet<TipoBanco> TiposBancos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Properties().Where(p => p.Name == p.ReflectedType.Name + "Id")
                .Configure(p => p.IsKey());

            modelBuilder.Properties<String>().Configure(p => p.HasColumnType("varchar"));
            modelBuilder.Properties<String>().Configure(p => p.HasMaxLength(250));

            #region EntityConfiguration
            modelBuilder.Configurations.Add(new TipoBancoConfiguration());
            #endregion
        }

        public override int SaveChanges()
        {
            foreach(var entry in ChangeTracker.Entries().Where
                (entry => entry.GetType().GetProperty("DataCreation") != null))
            {
                if(entry.State == EntityState.Added)
                {
                    entry.Property("DataCreation").CurrentValue = System.DateTime.Now;
                }

                if(entry.State == EntityState.Modified)
                {
                    entry.Property("DataModification").CurrentValue = System.DateTime.Now;
                }
            }
            return base.SaveChanges();
        }
    }
}

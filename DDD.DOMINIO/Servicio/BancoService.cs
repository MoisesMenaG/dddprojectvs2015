﻿using DDD.DOMINIO.Entidad;
using DDD.DOMINIO.Interface;
using DDD.DOMINIO.Interface.Service;

namespace DDD.DOMINIO.Servicio
{
    public class BancoService: ServiceBase<Banco>, IBancoService
    {
        private readonly IBancoRepository _bancoRepositoy;

        public BancoService(IBancoRepository bancoRepository)
            :base(bancoRepository)
        {
            _bancoRepositoy = bancoRepository;
        }
    }
}

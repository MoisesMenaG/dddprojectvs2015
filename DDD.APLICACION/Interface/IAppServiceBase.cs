﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.APLICACION.Interface
{
    public interface IAppServiceBase<TEntity> where TEntity:class
    {
        void Add(TEntity obj);
        void Update(TEntity obj);
        void Remove(TEntity obj);
        TEntity GetById(int id);
        void Dispose();
        IEnumerable<TEntity> GetAll();
        IQueryable<TEntity> GetAllQuery();
    }
}

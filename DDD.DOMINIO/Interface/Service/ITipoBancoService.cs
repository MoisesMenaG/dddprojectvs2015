﻿using DDD.DOMINIO.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.DOMINIO.Interface.Service
{
    public interface ITipoBancoService: IServiceBase<TipoBanco>
    {
    }
}
